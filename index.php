<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Router::get('', 'DefaultController');
Router::get('plan', 'PlanController');
Router::post('addTask', 'PlanController');
Router::post('clearTasks', 'PlanController');
Router::get('signup', 'DefaultController');
Router::get('meet', 'MeetController');
Router::get('shop', 'ShopController');
Router::post('addProduct', 'ShopController');
Router::post('deleteShop', 'ShopController');
Router::post('addShop', 'ShopController');
Router::post('login', 'SecurityController');
Router::post('logout', 'SecurityController');
Router::post('addMeet', 'MeetController');
Router::post('signup', 'SecurityController');
Router::post('deleteButton', 'MeetController');

Router::run($path);