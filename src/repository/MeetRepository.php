<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/Meet.php';

class MeetRepository extends Repository
{
    public function addMeet(Meet $meet): void
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO meet (photo, name_and_surname, date, hour, place, id_user)
            VALUES (?, ?, ?, ?, ?, ?)
        ');


        $stmt->execute([
            $meet->getPhoto(),
            $meet->getNameAndSurname(),
            $meet->getDate(),
            $meet->getHour(),
            $meet->getPlace(),
            $meet->getIdUser()
        ]);
    }

    public function getMeets(): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
        SELECT * FROM public.meet WHERE id_user=:id;
        ');
        $id = intval($_COOKIE['id']);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $meets = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($meets as $meet) {
            $result[] = new Meet (
                $meet['photo'],
                $meet['name_and_surname'],
                $meet['date'],
                $meet['hour'],
                $meet['place'],
                $id,
                $meet['id']
            );
        }

        return $result;
    }

    public function deleteMeet(int $id): bool
    {
        $stmt = $this->database->connect()->prepare('
        DELETE FROM public.meet WHERE id=:id_meet;
        ');

        $stmt->bindParam(':id_meet', $id, PDO::PARAM_INT);
        return $stmt->execute();
    }
}