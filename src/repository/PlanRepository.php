<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/Plan.php';

class PlanRepository extends Repository
{

    public function getPlan(int $id): ?Plan
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.task WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $plan = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($plan == false) {
            return null;
        }
        return new Plan(
            $plan['day'],
            $plan['description'],
            $plan['created_at']
        );
    }

    public function addPlan(Plan $plan): void
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO task (day,description,created_at)
            VALUES (?,?,?)
        ');

        $stmt->execute([
            $plan->getDay(),
            $plan->getDescription(),
            $plan->getCreatedAt()
        ]);
    }

    public function getPlans(): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
        SELECT * FROM task as t
        LEFT JOIN public.user_days ud
        ON ud.id_user_day = t.id_day
        WHERE ud.id_user=:id
        ORDER BY ud.day ASC
        ');
        $id = $_COOKIE["id"];
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $plans = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $old_id = -1;
        $task_object = new Plan(0);

        foreach ($plans as $plan) {
            $day = intval($plan["day"]);
            if ($day != $old_id) {
                $plan_object = new Plan($day);
                $result[$day] = $plan_object;
                $old_id = $day;
            }
            $plan_object->addPlans($plan['description']);
        }

        return $result;
    }

    public function addTask(int $day, string $task):bool {

        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.task (description, id_day)
            VALUES (?, ?)
        ');

        return $stmt->execute([
                $task,
                $this->getUserDay($day)
        ]);

    }

    public function createDays(int $id_user)
    {
        $day=0;
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.user_days (id_user, day)
            VALUES (?, ?)
        ');
        for($day=0; $day<3; $day++)
        {
            $stmt->execute([
                $id_user,
                $day
            ]);
        }
    }

    public function clearTasks(int $day){
        $stmt = $this->database->connect()->prepare('
            DELETE FROM public.task WHERE id_day = :day
        ');
        $user_day = $this->getUserDay($day);
        $stmt->bindParam(':day',$user_day , PDO::PARAM_INT);
        return $stmt->execute();
    }
    private function getUserDay(int $day){
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.user_days WHERE id_user = :id AND day = :day
        ');
        $stmt->bindParam(':id', $_COOKIE['id'], PDO::PARAM_INT);
        $stmt->bindParam(':day', $day, PDO::PARAM_INT);
        $stmt->execute();

        $response = $stmt->fetch(PDO::FETCH_ASSOC);
        return $response["id_user_day"];
    }
}