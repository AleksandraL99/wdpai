<?php


class ShopRepository extends Repository
{

    public function getShop(int $id): ?Shop
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.product WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $shop = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($shop == false) {
            return null;
        }
        return new Plan(
            $shop['name'],
            $shop['list_name']
        );
    }

    public function addShop($name, $products): bool
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.shop_list (name,id_user)
            VALUES (?,?)
        ');

        $state = $stmt->execute([
            $name,
            $_COOKIE["id"]
        ]);


        if ($state) {
            $id_list = $this->getShopId($name, intval($_COOKIE["id"]));


            foreach ($products as $product) {
                $stmt = $this->database->connect()->prepare('
            INSERT INTO public.product (product_name,id_list)
            VALUES (?,?)
            ');

                $state = $stmt->execute([
                    $product,
                    $id_list
                ]);
            }
        }
        return $state;
    }

    public function getShopId(string $name, string $id): int
    {

        $stmt = $this->database->connect()->prepare('
        SELECT * FROM public.shop_list WHERE name=:name AND id_user=:id
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->execute();
        $response = $stmt->fetch(PDO::FETCH_ASSOC);
        return $response["id_shop_list"];
    }

    public function getShops(): array
    {
        $result = [];

        $stmt = $this->database->connect()->prepare('
        SELECT * FROM public.shop_list sl 
        LEFT JOIN product as p 
        ON sl.id_shop_list = p.id_list 
        WHERE sl.id_user=:id
        ');
        $id = $_COOKIE["id"];
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $shops = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $old_id = "";
        $shop_object = new Shop("", 1);

        foreach ($shops as $shop) {
            if ($shop["id_shop_list"] != $old_id) {
                $shop_object = new Shop($shop['name'], intval($shop['id_shop_list']));
                array_push($result, $shop_object);
                $old_id = $shop["id_shop_list"];
            }
            $shop_object->addProducts($shop['product_name']);
        }

        return $result;
    }

    public function deleteShop(int $id): bool
    {
        $stmt = $this->database->connect()->prepare('
        DELETE FROM public.shop_list WHERE id_shop_list=:id;
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        return $stmt->execute();
    }

    public function addProduct(int $id, string $shop_name):bool{
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.product (product_name,id_list)
            VALUES (?,?)
        ');

        return $stmt->execute([
            $shop_name,
            $id
        ]);


    }
}