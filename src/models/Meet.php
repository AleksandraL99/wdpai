<?php

class Meet {
    private $name_and_surname;
    private $date;
    private $hour;
    private $place;
    private $photo;
    private $id_user;
    private $meet_id;


    public function __construct($photo, $name_and_surname, $date, $hour, $place, $id_user, $meet_id=1)
    {
        $this->photo = $photo;
        $this->name_and_surname = $name_and_surname;
        $this->date = $date;
        $this->hour = $hour;
        $this->place = $place;
        $this->id_user = $id_user;
        $this->meet_id = $meet_id;
    }

    public function getNameAndSurname()
    {
        return $this->name_and_surname;
    }

    public function setNameAndSurname($name_and_surname): void
    {
        $this->name_and_surname = $name_and_surname;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date): void
    {
        $this->date = $date;
    }

    public function getHour()
    {
        return $this->hour;
    }

    public function setHour($hour): void
    {
        $this->hour = $hour;
    }

    public function getIdUser()
    {
        return $this->id_user;
    }

    public function setIdUser($id_user): void
    {
        $this->id_user = $id_user;
    }

    public function getPlace()
    {
        return $this->place;
    }

    public function setPlace($place): void
    {
        $this->place = $place;
    }


    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    public function getMeetId()
    {
        return $this->meet_id;
    }

    public function setMeetId($meet_id): void
    {
        $this->meet_id = $meet_id;
    }
}