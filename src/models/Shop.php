<?php


class Shop
{
    private $name;
    private array $products;
    private $id;


    public function __construct($name, $id)
    {
        $this->id = $id;
        $this->name = $name;
        $this->products = [];
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function addProducts(string $product): void
    {
        array_push($this->products,$product);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

}