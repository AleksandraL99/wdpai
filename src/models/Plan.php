<?php

class Plan
{
    private $id_day;
    private array $tasks;

    public function __construct($id_day)
    {
        $this->id_day = $id_day;
        $this->tasks = [];
    }

    public function addPlans($description)
    {
        array_push($this->tasks,$description);
    }

    public function getPlans(): array
    {
    return $this->tasks;
    }

    public function getIdDay()
    {
        return $this->id_day;
    }

    public function setIdDay($id_day): void
    {
        $this->id_day = $id_day;
    }
}