<?php

require_once 'AppController.php';
require_once __DIR__ . '/../models/Shop.php';
require_once __DIR__ . '/../repository/ShopRepository.php';

class ShopController extends AppController
{
    private $message = [];
    private $shopRepository;

    public function __construct()
    {
        parent::__construct();
        $this->shopRepository = new ShopRepository();
    }

    public function shop()
    {
        if(!isset($_COOKIE["email"])) {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/login");
        }

        $shops  = $this->shopRepository->getShops();
        $this->render('shop',['shops'=>$shops]);
    }

    public function deleteShop(){
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        if ($contentType === "application/json") {
            $decoded = $this->contentAndDecode();
            $id = $decoded["id"];
            $id = intval($id);
            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode($this->shopRepository->deleteShop($id));
        }
    }


    public function addShop()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if ($contentType === "application/json") {
            $decoded = $this->contentAndDecode();
            $name = $decoded["name"];
            $products = $decoded["products"];
            header('Content-type: application/json');
            http_response_code(200);

            $response["response"] = $this->shopRepository->addShop($name,$products);
            $response["id_shop"] = $this->shopRepository->getShopId($name,intval($_COOKIE['id']));

            echo json_encode($response);
        };
    }

    public function addProduct(){
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        if ($contentType === "application/json") {
            $decoded = $this->contentAndDecode();
            $id = intval($decoded["id_shop"]);
            $shop_name = $decoded["product"];

            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode($this->shopRepository->addProduct($id,$shop_name));
        }
    }


}

