<?php

require_once 'AppController.php';
require_once __DIR__ . '/../models/Plan.php';
require_once __DIR__ . '/../repository/PlanRepository.php';

class PlanController extends AppController
{
    private $message = [];
    private $planRepository;

    public function __construct()
    {
        parent::__construct();
        $this->planRepository = new PlanRepository();
    }

    public function plan()
    {
        if(!isset($_COOKIE["email"])) {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/login");
        }

        $plans = $this->planRepository->getPlans();

        $this->render('plan',['plans'=>$plans]);

    }

    public function addTask(){
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        if ($contentType === "application/json") {
            $decoded = $this->contentAndDecode();
            $day = intval($decoded["day"]);
            $task = $decoded["task"];

            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode($this->planRepository->addTask($day,$task));
        }
    }

    public function clearTasks(){
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        if ($contentType === "application/json") {
            $decoded = $this->contentAndDecode();
            $day = intval($decoded["day"]);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->planRepository->clearTasks($day));
        }
    }

}
