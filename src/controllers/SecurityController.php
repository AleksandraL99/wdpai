<?php

require_once 'AppController.php';
require_once __DIR__ . '/../models/User.php';
require_once __DIR__ . '/../repository/UserRepository.php';
require_once __DIR__ . '/../repository/PlanRepository.php';

class SecurityController extends AppController
{

    private $userRepository;
    private $planRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->planRepository = new PlanRepository();
    }

    public function login()
    {
        if (!$this->isPost()) {
            return $this->render('login');
        }

        $email = $_POST['email'];
        $password = md5($_POST['password']);

        $user = $this->userRepository->getUser($email);

        if (!$user) {
            return $this->render('login', ['messages' => ['Brak uzytkownika o takim emailu']]);
        }

        if ($user->getPassword() !== $password) {
            return $this->render('login', ['messages' => ['Haslo niepoprawne']]);
        }

        setcookie("email", $user->getEmail(), time() + (86400 * 30));
        setcookie("id", $user->getId(), time() + (86400 * 30));


        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/plan");
    }

    public function signup()
    {
        if (!$this->isPost()) {
            return $this->render('signup');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];

        if ($password !== $confirmedPassword) {
            return $this->render('signup', ['messages' => ['Please provide proper password']]);
        }

        $user = new User($email, md5($password), $name, $surname);

        $stmt = $this->userRepository->addUser($user);
        if ($stmt) {
            return $this->render('signup', ['messages' => ['Nie udało się zarejestrować. Spróbuj ponownie!']]);
        }

        $this->planRepository->createDays($this->userRepository->getUser($email)->getId());

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/plan");

    }

    public function logout()
    {
        setcookie("email", "", time() - 10000);
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/login");
    }
}