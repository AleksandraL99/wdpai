<?php

require_once 'AppController.php';
require_once __DIR__ . '/../models/Meet.php';
require_once __DIR__ . '/../repository/MeetRepository.php';

class MeetController extends AppController
{

    const MAX_FILE_SIZE = 512 * 512;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $message = [];
    private $meetRepository;

    public function __construct()
    {
        parent::__construct();
        $this->meetRepository = new MeetRepository();
    }

    public function meet()
    {
        if(!isset($_COOKIE["email"])) {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/login");
        }
        $meets = $this->meetRepository->getMeets();
        $this->render('meet', ['meets'=> $meets]);
    }

    public function addMeet()
    {
        $ext = pathinfo($path, PATHINFO_EXTENSION); */
        if ($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])) {
            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_FILES['file']['name']
            );

            $meet = new Meet($_FILES['file']['name'], $_POST['name_and_surname'], $_POST['date'], $_POST['hour'], $_POST['place'], $_COOKIE['id']);
            $this->meetRepository->addMeet($meet);

            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/meet");
        }
        return $this->render('meet', ['messages' => $this->message]);
    }

    private function validate(array $file): bool
    {
        if ($file['size'] > self::MAX_FILE_SIZE) {
            $this->message[] = 'File is too large for destination file system.';
            return false;
        }

        if (!isset($file['type']) || !in_array($file['type'], self::SUPPORTED_TYPES)) {
            $this->message[] = 'File type is not supported.';
            return false;
        }
        return true;
    }

    public function deleteButton()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if ($contentType === "application/json") {
            $decoded = $this->contentAndDecode();
            $id = $decoded["id"];
            $id = intval($id);

            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode($this->meetRepository->deleteMeet($id));
        }
    }
}
