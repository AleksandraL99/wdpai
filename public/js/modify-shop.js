


const frame_container = document.querySelector(".division_shop");

document.querySelector("#add_new").addEventListener("click", function () {

    const new_frame = '<div class="frame">       ' +
        '<div id="part1">' +
        '            <input name="shop" type="text" placeholder="Nazwa sklepu">' +
        '        </div>' +
        '        <hr>' +
        '            <div id="part2">' +
        '                <input name="product" rows=15 placeholder="Wpisz pierwszy produkt..."></input>' +
        '<div id="product_list"></div> ' +
        '    </div>' +

        '    <hr>' +
        '                <div id="part3">' +
        '            <div id="button_place">' +
        '                <button id="confirm" type="submit">Zatwierdź</button>' +
        '                <button class="clear" type="button">Usuń</button>' +
        '            </div>' +
        '        </div>' +
        '    </div>';

    frame_container.innerHTML += new_frame;
    document.querySelector("#confirm").addEventListener("click", confirm);

    let new_product = document.querySelectorAll("input[name='product']");
    new_product = new_product[new_product.length-1];
    new_product.addEventListener("keypress", addProduct)

})

addEnterListener();
function addEnterListener(){
    const product_block = document.querySelectorAll("input[name='product']");
    for (let i = 0; i < product_block.length; i++) {
        product_block[i].addEventListener("keypress", addProduct)
    }
}

function addProduct(e) {

    if (e.key === "Enter") {
        const element = this;
        const container = this.parentNode.parentNode;
        const name_container = container.querySelector("#part1").children[0]
        let id_ = container.id;
        let tab = id_.slice(6);

        if(name_container.tagName === "P"){
            let data = {
                id_shop:tab,
                product:  element.value
            };
            fetch("/addProduct", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }).then(function (response) {
                return response.json();
            }).then(function (response) {
                if (response) {
                    const product_list = container.querySelector("#product_list");
                    product_list.innerHTML += "<p>" + element.value + "</p>";
                    element.value = "";
                }else{
                    alert("Problem z dodaniem produktu!");
                }
            });
        }else{

            const product_list = container.querySelector("#product_list");
            product_list.innerHTML += "<p>" + element.value + "</p>";
            element.value = "";
        }
    }

}

function confirm() {
    const element = this;
    const container = this.parentNode.parentNode.parentNode;
    const shop = container.querySelector("input[name='shop']");
    const product_list = container.querySelector("#product_list");
    if (shop.value.length >= 2 && product_list.childNodes.length > 0) {

        let products = [];
        for (let i = 0; i < product_list.childNodes.length; i++) {
            products.push(product_list.childNodes[i].textContent)
        }
        let data = {
            name: shop.value,
            products: products
        }

        fetch("/addShop", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
            return response.json();
        }).then(function (response) {

            if(response.response){
                document.querySelector("#confirm").remove();
                shop.parentNode.innerHTML+=`<p>${shop.value}</p>`;
                container.querySelector("input[name='shop']").remove();
                container.querySelector(".clear").setAttribute("id", `clear@${response.id_shop}`);
                container.setAttribute("id", `frame@${response.id_shop}`);
                addDeleteListener();

            }else{
                alert("Nie dodano do bazy danych!")
            }


        });
    } else {
        alert("Nazwij swoj sklep i dodaj produkty!");
    }



}

addDeleteListener();
function addDeleteListener(){
    const delete_buttons = document.querySelectorAll(".clear");

    for (let i = 0; i < delete_buttons.length; i++) {
        delete_buttons[i].addEventListener("click", delete_)
    }
}


function delete_() {

    let element = this;
    let id_ = this.id;
    let tab = id_.slice(6);
    let data = {id: parseInt(tab)};
    fetch("/deleteShop", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(function (response) {
        return response.json();
    }).then(function (response) {
        if (response) {
            element.parentNode.parentNode.parentNode.remove()
        }
    });
}