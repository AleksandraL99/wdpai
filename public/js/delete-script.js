let delete_buttons = document.querySelectorAll(".clear");

for(let i=0;i<delete_buttons.length;i++)
{
    delete_buttons[i].addEventListener("click",  delete_);
}

function delete_() {
    let element = this;
    let id_ = this.id;
    let tab = id_.slice(5);
    let data = {id:parseInt(tab)};

    fetch("/deleteButton", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(function (response) {
        return response.json();
    }).then(function (response) {
        if(response){
            element.parentNode.parentNode.parentNode.remove()
        }
    });
}