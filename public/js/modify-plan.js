const inputs = document.querySelectorAll("input[name='write_plan']");
for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener("keypress", addTask)
}

function addTask(e) {
    if (e.key === "Enter") {
        const element = this;
        const container = this.parentNode.parentNode.parentNode;
        const day_list = container.querySelector("#day_list");

        if (element.value.length > 2) {

            let id_ = element.id;
            let tab = id_.slice(3);
            let data = {
                day: tab,
                task: element.value
            };
            fetch("/addTask", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }).then(function (response) {
                return response.json();
            }).then(function (response) {
                if (response) {
                    day_list.innerHTML += "<p>" + element.value + "</p>";
                    element.value = "";
                } else {
                    alert("Problem z dodaniem produktu!");
                }
            });
        } else {
            alert("Nic nie wpisano!");
        }

    }

}


const clear_buttons = document.querySelectorAll(".clear");

for (let i = 0; i < clear_buttons.length; i++) {
    clear_buttons[i].addEventListener("click", clearTasks)
}
function clearTasks() {
    const element = this;
    const container = this.parentNode.parentNode.parentNode;
    const day_list = container.querySelector("#day_list");

    let id_ = element.id;
    let tab = id_.slice(6);
    let data = {
        day: tab
    };
    fetch("/clearTasks", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(function (response) {
        return response.json();
    }).then(function (response) {
        if (response) {
            container.querySelector("#day_list").innerHTML=""
        } else {
            alert("Problem z dodaniem produktu!");
        }
    });
}