<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/mobile.css">
    <script type="text/javascript" src="./public/js/script.js" defer></script>
    <title>LOGIN PAGE</title>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="login-container">
            <form class="login" action="login" method="POST">
                <div class="messages">
                    <?php if(isset($messages)) {
                        foreach ($messages as $message) {
                            echo $message;
                        }
                        }
                        ?>
                </div>
                <input name="email" type="text" placeholder="email">
                <input name="password" type="password" placeholder="haslo">
                <div class="button-container">
                    <button type="submit">zaloguj</button>
                    <a href="signup"><button type="button">utwórz konto</button></a>
                </div>
            </form>
        </div>
    </div>
</body>