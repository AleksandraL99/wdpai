<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/signup.css">
    <link rel="stylesheet" type="text/css" href="public/css/mobile.css">
    <script type="text/javascript" src="./public/js/script.js" defer></script>
    <title>LOGIN PAGE</title>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="sign-container">
            <form class="sign" action="signup" method="POST">
                <div class="messages">
                    <?php
                    if(isset($messages)){
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <input name="name" type="text" placeholder="imię">
                <input name="surname" type="text" placeholder="nazwisko">
                <input name="email" type="text" placeholder="email">
                <input name="password" type="password" placeholder="hasło">
                <input name="confirmedPassword" type="password" placeholder="powtórz hasło">
                <div class="button-container">
                    <button>zarejestruj</button>
                </div>
            </form>
        </div>
    </div>
</body>