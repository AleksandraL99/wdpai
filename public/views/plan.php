<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="./public/css/style.css">
    <link rel="stylesheet" type="text/css" href="./public/css/subpages.css">
    <link rel="stylesheet" type="text/css" href="public/css/mobile.css">
    <script type="text/javascript" src="./public/js/modify-plan.js" defer></script>
    <script src="https://kit.fontawesome.com/701fc5c81a.js" crossorigin="anonymous"></script>
    <title>PLAN</title>
</head>

<body>
    <div class="base-container">
        <main>
            <div class="plan_division">
                <div id="today">
                    <div id="inscription">
                        Dzisiaj
                    </div>
                    <div id="frame">
                        <div id="place_to_write">
                            <input id="day0" name="write_plan" type="text" placeholder="Zaplanuj coś...">
                            <div id='day_list'>
                                <?php
                                if(isset($plans[0]) ){
                                    foreach($plans[0]->getPlans() as $task) {
                                        echo "<p>{$task}</p>";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <hr>
                        <div id="button_place">
                            <button class="clear" id="clear@0">Wyczyść</button>
                        </div>
                    </div>
                </div>
                <div id="tomorrow">
                    <div id="inscription">
                        Jutro
                    </div>
                    <div id="frame">
                        <div id="place_to_write">
                            <input  id="day1" name="write_plan" type="text" placeholder="Zaplanuj coś...">
                            <div id='day_list'>
                                <?php

                                if(isset($plans[1])){
                                    foreach($plans[1]->getPlans() as $task) {
                                        echo "<p>{$task}</p>";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <hr>
                        <div id="button_place">
                            <button class="clear" id="clear@1">Wyczyść</button>
                        </div>
                    </div>
                </div>
                <div id="day_after_tomorrow">
                    <div id="inscription">
                        Pojutrze
                    </div>
                    <div id="frame">
                        <div id="place_to_write">
                            <input  id="day2" name="write_plan" type="text" placeholder="Zaplanuj coś...">
                            <div id='day_list'>
                                <?php
                                if(isset($plans[2])){
                                    foreach($plans[2]->getPlans() as $task) {
                                        echo "<p>{$task}</p>";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <hr>
                        <div id="button_place">
                            <button class="clear" id="clear@2">Wyczyść</button>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <nav>
            <img src="public/img/logo.svg">
            <ul>
                <li>
                    <a href="#" class="button"><i class="far fa-calendar-alt"></i> <p1>Plan</p1></a>
                </li>
                <li>
                    <a href="shop" class="button"> <i class="fas fa-shopping-cart"></i> <p1>Zakupy</p1></a>
                </li>
                <li>
                    <a href="meet" class="button"><i class="fas fa-user-friends"></i> <p1>Spotkania</p1></a>
                </li>
                <li>
                    <a href="logout" class="button"><i class="fas fa-sign-out-alt"></i>
                        <p1>Wyloguj</p1>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</body>