<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/subpages.css">
    <link rel="stylesheet" type="text/css" href="public/css/mobile.css">
        <script type="text/javascript" src="./public/js/modify-shop.js" defer></script>
    <script src="https://kit.fontawesome.com/701fc5c81a.js" crossorigin="anonymous"></script>
    <title>PLAN</title>
</head>

<body>
<div class="base-container">
    <main>
        <div class="division_shop">
            <?php if(isset($shops)){
                foreach ($shops as $shop){
                    echo "
           
                <div class='frame' id='frame@{$shop->getId()}'>
                    <div id='part1'>
                        <p>{$shop->getName()} </p>
                    </div>
                    <hr>
                    <div id='part2'>
                        <input name='product' rows=15 placeholder='Wpisz pierwszy produkt...'></input>
                        <div id='product_list'>";
                    foreach ($shop->getProducts() as $product){
                        echo "<p>{$product}</p>";
                    }
                    echo "</div>
                    </div>
                    <hr>
                    <div id='part3'>
                        <div id='button_place'>
                            <button class='clear' type='button' id='clear@{$shop->getId()}'>Usuń</button>
                        </div>
                    </div>
                </div>";

                }
            }

            ?>

        </div>
        <div id="underside">
            <button id="add_new">
                <i class="fas fa-plus"></i>
                Dodaj
            </button>
        </div>
    </main>
        <nav>
            <img src="public/img/logo.svg">
            <ul>
                <li>
                    <a href="plan" class="button"><i class="far fa-calendar-alt"></i> <p1>Plan</p1></a>
                </li>
                <li>
                    <a href="#" class="button"> <i class="fas fa-shopping-cart"></i> <p1>Zakupy</p1></a>
                </li>
                <li>
                    <a href="meet" class="button"><i class="fas fa-user-friends"></i> <p1>Spotkania</p1></a>
                </li>
                <li>
                    <a href="logout" class="button"><i class="fas fa-sign-out-alt"></i>
                        <p1>Wyloguj</p1>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</body>