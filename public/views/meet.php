<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/subpages.css">
    <link rel="stylesheet" type="text/css" href="public/css/mobile.css">
    <script type="text/javascript" src="./public/js/delete-script.js" defer></script>
    <script src="https://kit.fontawesome.com/701fc5c81a.js" crossorigin="anonymous"></script>
    <title>PLAN</title>
</head>

<body>
<div class="base-container">
    <main>
        <div class="division">

            <section class="meets">
                <?php if (isset($meets)) {

                    foreach ($meets as $meet) {
                        echo "<div id='meet-1'>
                            <div id='frame'>
                                <div id='photo'>
                                     <img src='public/uploads/{$meet->getPhoto()}'>
                                </div>
                                <hr>
                                <div id='information'>
                                     <p>{$meet->getNameAndSurname()}</p>
                                     <p>{$meet->getDate()}</p>
                                     <p>{$meet->getHour()}</p>
                                     <p>{$meet->getPlace()}</p>
                                </div>
                                <hr>
                                <div id='button_place'>
                                    <div id='clear{$meet->getMeetId()}' class='clear'>Usuń</div>
                                </div>
                            </div>
                        </div>";
                    }

                } ?>

            <div id="person">
                <div id="frame">
                    <form action="addMeet" method="POST" ENCTYPE="multipart/form-data">
                        <div class="messages">
                            <?php
                            if (isset($messages)) {
                                foreach ($messages as $message) {
                                    echo $message;
                                }
                            }
                            ?>
                        </div>
                        <div id="photo">
                            <input id="add_photo" type="file" name="file"/><br/>
                        </div>
                        <hr>
                        <div id="information">
                            <input name="name_and_surname" type="text" placeholder="Imię Nazwisko">
                            <input name="date" type="text" placeholder="Data spotkania">
                            <input name="hour" type="text" placeholder="Godzina spotkania">
                            <input name="place" type="text" placeholder="Miejsce spotkania">
                        </div>
                        <hr>
                        <div id="button_place">
                            <button id="confirm" type="submit">Zatwierdź</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </section>
    </main>
    <nav>
        <img src="public/img/logo.svg">
        <ul>
            <li>
                <a href="plan" class="button"><i class="far fa-calendar-alt"></i>
                    <p1>Plan</p1>
                </a>
            </li>
            <li>
                <a href="shop" class="button"> <i class="fas fa-shopping-cart"></i>
                    <p1>Zakupy</p1>
                </a>
            </li>
            <li>
                <a href="#" class="button"><i class="fas fa-user-friends"></i>
                    <p1>Spotkania</p1>
                </a>
            </li>
            <li>
                <a href="logout" class="button"><i class="fas fa-sign-out-alt"></i>
                    <p1>Wyloguj</p1>
                </a>
            </li>
        </ul>
    </nav>
</div>
</body>